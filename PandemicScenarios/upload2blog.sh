#!/bin/bash

set -o pipefail -o errexit -o nounset -o xtrace
cd $(dirname ${BASH_SOURCE[0]})

zip PandemicScenariosByLucasCimon.zip Fonts background*.jpg map.png *.fodg *.odg pandemic-logo.png thumbnail*.jpg

for file in *druckfreundlich.jpg *.pdf *.zip; do
  scp "$file" ct-lucas:~/ludochaordic/content/images/jeux/
done
