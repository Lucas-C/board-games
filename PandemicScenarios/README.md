_cf._ <https://chezsoi.org/lucas/blog/pandemic-worldwide-research-program-and-mass-migrations.html>

    ./upload2blog.sh

## Programme de recherche mondial
2019/11/18: 2 playests, avec 5 puis 4 épidémies.
=> les 2 perdus (éclosion & plus assez de cartes rouges pour vaccin)
=> modif de la variante (plus d'action pour récupérer une carte

## Mass migrations
2019/09/30: 2 playests concluants: 1 fail & 1 success, les 2 avec 5 épidémies
=> ne révolutionne pas le jeu mais ajoute + d'aléatoire

## Images
Most were extracted from the official rules PDF

## Fonts
`scenarios_pandemic.pdf`:

    Eurostile-Demi
    Eurostile-DemiOblique
    Helvetica
        -Condensed-Black
        -Condensed-BlackObl
        -Condensed-Light
    HelveticaNeue
        -BlackItalic
        -Condensed-Bold
    LowGunScreen
    LowGunScreenBold
