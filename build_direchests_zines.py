#!/usr/bin/env python3
# Content source: https://oddgobgames.itch.io/legacy-content-pack
# Install step: pip install fpdf2
# Preliminary step: cd "DIRECHESTS/DECEMBER 2020" && mv "page images" Pages
from glob import glob
import os
from fpdf import FPDF
from PIL import Image

CUR_DIR = os.path.dirname(__file__)

for dir in glob(f"{CUR_DIR}/DIRECHESTS/*"):
    images = list(glob(f"{dir}/Pages/*"))
    pdf = FPDF(format=(Image.open(images[0]).size))
    pdf.set_margin(0)
    pdf.set_auto_page_break(False)
    for img in images:
        pdf.add_page()
        pdf.image(img, w=pdf.epw, h=pdf.eph, keep_aspect_ratio=True)
    pdf_file_path = dir[2:].replace("/", "-").replace(" ", "-") + ".pdf"
    pdf.output(pdf_file_path)
    print(f"Generated: {pdf_file_path}")
