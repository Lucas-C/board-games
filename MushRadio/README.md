<https://fr.tipeee.com/mush-radio>

![](promo/MushRadio-2020-06-19-noloop.gif)

- <https://lucas-c.gitlab.io/board-games/MushRadio/>
- <https://lucas-c.gitlab.io/board-games/MushRadio/history/>

<!-- NEXT steps :
* faire imprimer des sets de grilles -> demander devis
    - nantes@copytop.com
    - & https://www.corep.fr/devis-personnalise/
    - info@super-imprim.fr - Centre Commercial "L'Esplanade"
    Comparatif blocs A4
        veoprint.com : 127€ pour 50 blocs de 50 feuillets (80g/m²)
        www.lesgrandesimprimeries.com : 302€ pour 100 blocs de 50 feuillets (90g, dos carton & tête de bloc encollée)
* contacter éditeur Matagot ?
  Anne m'a recommandé Opla niveau éco-responsabilité : https://www.jeux-opla.fr
* identifier des events où présenter le jeu : cf. Evenements.md + CONTACTER DES EDITEURS!
* com' physique : print carte promo à eVTech & l'afficher dans boulangerie St Math & vergers St Rémy
* retours post-release :
  * suggestion Vincent Marier: QR code dans le PDF de règle, qui redirige vers la page où télécharger les grilles ?
  * Thomas: est-on obligé d'utiliser une catapulte lorsque l'on en a une ?
* license à terme : CC BY-NC-ND-SA
* nouveaux playtesteurs: Juliette & PE, @LaMénitrée, David Franck & Martin @ eVTech
  + afficher une mini affiche dans la kfet pour proposer des playtests de Mush Radio
* photo de la feuille de jeu avec figurine ?
* vidéo de présentation du jeu -> envoyer des exemples à Elliot + trouver des musiques libre de droits
  + https://dos88.itch.io/dos-88-music-library
* trad anglaise : Google Docs -> puis envoyer à Elliot
-->
