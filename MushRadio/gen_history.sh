#!/bin/bash

set -o pipefail -o errexit -o nounset -o xtrace
cd $(dirname ${BASH_SOURCE[0]})

rm -rf history && mkdir history

INDEX_HTML=
# TODO: check if "git tag --list" works well in pipeline
for tag in $(git tag --list); do
    git checkout $tag
    mkdir -p history/$tag
    cp -rt history/$tag $(git ls-files | grep -Ev '.md$|.gif$|.webm$|v0.3.html' | sed 's~/\.+~~')
    INDEX_HTML="$INDEX_HTML<li><a href=\"./$tag\">$tag - $(git show --no-patch --format='%cd' $tag^{commit})</a></li>"$'\n'
done

git checkout master
cat >history/index.html <<EOF
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mush Radio versions history</title>
  </head>
  <body>
    <h1>Mush Radio versions history</h1>
    <ul>
    $INDEX_HTML
    </ul>
  </body>
</html>
EOF
