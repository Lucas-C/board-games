/* Support for ?nodice secret query param: */
const queryParams = new URLSearchParams(location.search);
if (queryParams.has('nodice')) {
  // document.getElementById('setup').style = 'block';
  // document.getElementById('heatmap').style = 'block';
  document.getElementById('propagation').onclick = propagation;
  document.getElementById('propagation').classList.add('active');
  document.getElementById('essaimage').onclick = essaimage;
  document.getElementById('essaimage').classList.add('active');
  document.getElementById('eclosion').onclick = eclosion;
  document.getElementById('eclosion').classList.add('active');
  document.querySelectorAll('#fongus td').forEach(td => {
    td.addEventListener('mouseenter', fongusTableMouseEnter);
    td.addEventListener('mouseleave', fongusTableMouseLeave);
  });
}
function fongusTableMouseEnter(event) {
  const textCoords = event.target.textContent;
  parseTextCoords(textCoords).forEach(([i, j]) => {
    squareAtPos(i, j).style.backgroundColor = 'blue';
  });
}
function fongusTableMouseLeave(event) {
  const textCoords = event.target.textContent;
  parseTextCoords(textCoords).forEach(([i, j]) => {
    squareAtPos(i, j).style.backgroundColor = 'initial';
  });
}

/* Grid generation entry point: */
const DICE = ['⚀', '⚁', '⚂', '⚃', '⚄', '⚅']
const BUILDING_CLASSES = ['base', 'foundry', 'refinery', 'filled']//'archives', 'catapult', 'lab', 'greenhouse', 'house', 'park', 'hospital', 'metro']
const MUSH_STATE_CLASSES = ['spore', 'infected', 'filled']//, 'cleaned']
const RESOURCE_CLASSES = ['resource', 'filled']
const BARRIER_CLASSES = ['line']//, 'hatched']//, 'crossed-line']
const playerGrid = document.getElementsByClassName('grid')[0];
drawGrid(playerGrid);
// const miniGrid = document.getElementsByClassName('grid')[1];
// drawGrid(miniGrid, /*mini=*/true);
Array.from(document.getElementById('resources').getElementsByClassName('box')).forEach(box => box.onclick = (event => cycleBoxClass(event, box, RESOURCE_CLASSES)));
const fongusSwarmingCoords = Array.from(document.getElementById('fongus').getElementsByTagName('tr')[1].getElementsByTagName('td'));
fongusSwarmingCoords.forEach(cell => cell.onclick = event => cycleBoxClass(event, cell, ['struck']));


/******************************************************************************/
/* Grid generation utilities:                                                 */
/******************************************************************************/
function drawGrid(grid, mini) {
  [...Array(7)].map((_, i) => {
    const tr = document.createElement('tr');
    [...Array(7)].map((_, j) => {
      const td = document.createElement('td');
      let square = td;
      if (mini) {
        square = document.createElement('div');
        td.appendChild(square);
      }
      if (!mini && i === 0 && j === 0) {
        square.classList.add('corner');
      } else if (j === 0 && 0 < i && i < 7) {
        square.classList.add('header');
        square.innerHTML = mini ? `<div>${i}` : `<div>${i} <span class="die">${DICE[i - 1]}</span></div>`;
      } else if (i === 0 && 0 < j && j < 7) {
        square.classList.add('header');
        const letter = String.fromCharCode(64 + j);
        square.innerHTML = mini ? `<div>${letter}` : `${letter} <span class="die">${DICE[j - 1]}</span>`;
      } else if (0 < i && i < 7 && 0 < j && j < 7) {
        square.classList.add('square');
        square.i = i;
        square.j = j;
        /*if (((2 <= i && i <= 5) && (j == 3 || j == 4))
         || ((3 == i || i == 4) && (2 <= j && j <= 5))) {
            square.classList.add('central');
        }*/
        /* Invisible relatively-positioned div allowing for .square>:first-child::after styling: */
        const whiteCrossHelper = document.createElement('div');
        whiteCrossHelper.classList.add('white-cross-helper');
        square.appendChild(whiteCrossHelper);
        const side = true;
        [...Array(2)].map((_, k) => {
          const box = document.createElement('div');
          if (!side) {
            box.classList.add('right');
          }
          box.classList.add('box', 'bare');
          if (k === 0) {
            //if (i === 5 && j === 3) {
            //  box.classList.add('park');
            //  box.onclick = event => cycleBoxClass(event, box, ['park', 'filled'], /*noBare=*/true);
            if (!k && (i === 1 || i === 6 || j === 1 || j === 6)) {  // No building on the outer rim
              box.classList.add('transparent');
            } else {
              box.classList.add('building');
              box.onclick = event => cycleBoxClass(event, box, BUILDING_CLASSES);
            }
          } else if (k === 1) {
            box.classList.add('fongus');
            box.onclick = event => cycleBoxClass(event, box, MUSH_STATE_CLASSES);
          } else if (k === 2) {
            box.classList.add('resource');
            //if (i === 5 && j === 3) { // park
            //  box.classList.add('filled');
            //} else {
              box.onclick = event => cycleBoxClass(event, box, RESOURCE_CLASSES);
            //}
          }
          square.appendChild(box);
        });
        if (!mini) {
          const squadHelper = document.createElement('div');
          squadHelper.classList.add('squad-helper');
          square.appendChild(squadHelper);
          square.onclick = () => {
            if (square.classList.contains('squad')) {
              square.classList.remove('squad');
            } else if (!document.getElementsByClassName('squad').length) {
              square.classList.add('squad');
            }
          };
        }
        if (i < 6) {
          const horizontalBarrier = document.createElement('div')
          horizontalBarrier.classList.add('barrier', 'horizontal', 'no-line');
          horizontalBarrier.onclick = event => cycleBoxClass(event, horizontalBarrier, BARRIER_CLASSES);
          square.appendChild(horizontalBarrier);
        }
        if (j < 6) {
          const verticalBarrier = document.createElement('div')
          verticalBarrier.classList.add('barrier', 'vertical', 'no-line');
          verticalBarrier.onclick = event => cycleBoxClass(event, verticalBarrier, BARRIER_CLASSES);
          square.appendChild(verticalBarrier);
        }
      }
      tr.appendChild(td);
    });
    grid.appendChild(tr);
  });
}
function cycleBoxClass(event, box, classes, noBare) {
  event.stopPropagation();
  const currentClass = box.classList[box.classList.length - 1];
  let i = classes.indexOf(currentClass);
  if (i >= 0) box.classList.remove(currentClass);
  i++;
  if (noBare && i >= classes.length) i = 0;
  if (i < classes.length) {
    box.classList.add(classes[i]);
  } else {
    box.classList.add('bare');
  }
  return false;
}


/******************************************************************************/
/* essaimage & propagation & eclosion :                                       */
/******************************************************************************/
function propagation() {
  Array.from(playerGrid.getElementsByClassName('infected')).forEach(infectedSporeBox =>
    accessibleNeighbours(infectedSporeBox.parentNode).forEach(([i, j]) => {
      const sporeBox = squareAtPos(i, j).getElementsByClassName('box')[1];
      const currentClass = sporeBox.classList[sporeBox.classList.length - 1];
      if (['bare', 'fongus'].includes(currentClass)) {
        addFongus(i, j, 'spore');
      }
    })
  );
}
function essaimage(event, fongusClass) {
  if (fongusSwarmingCoords.every(cell => cell.classList.contains('struck'))) {
    console.log('All spores have been added - no more swarming - game is over');
    return;
  }
  let dieRoll = getRandomInt(6);
  console.log(`Essaimage: die=${dieRoll + 1}`);
  fongusSwarmingCoords.forEach(td => td.style.backgroundColor = 'initial');
  while (fongusSwarmingCoords[dieRoll].classList.contains('struck')) {
    dieRoll = (dieRoll + 1) % 6;
  }
  const targetCell = fongusSwarmingCoords[dieRoll];
  targetCell.classList.add('struck');
  targetCell.style.backgroundColor = '#EA707E';
  parseTextCoords(targetCell.textContent).forEach(([i, j]) => {
    addFongus(i, j, fongusClass);
  });
}
function eclosion() {
  Array.from(document.getElementsByClassName('spore')).forEach(sporeBox => {
    const sporeSquare = sporeBox.parentNode;
    // No spore -> colony transformation for spores in the legend & in the squad perimeter:
    if (!sporeSquare.classList.contains('square') || !isInSquadPerimeter(sporeSquare.i, sporeSquare.j)) {
      sporeBox.classList.remove('spore');
      sporeBox.classList.add('infected');
    }
  });
}
function accessibleNeighbours(colony) { // Takes into account barriers
  const [i, j] = [colony.i - 1, colony.j - 1];
  return ['down', 'left', 'up', 'right'].map((direction) => {
    if (!hasBarrierInFront(i, j, direction)) {
      const [x, y] = {
        'right': [i + 1, j],
        'down': [i, j + 1],
        'left': [i - 1, j],
        'up': [i, j - 1],
      }[direction];
      if (0 <= x && x <= 5 && 0 <= y && y <= 5) return [x, y];
    }
  }).filter(coords => coords);
}
function hasBarrierInFront(i, j, direction) {
  const barrierType = ['right', 'left'].includes(direction) ? 'vertical' : 'horizontal';
  let barrier;
  if (direction === 'right' || direction === 'down') {
    barrier = squareAtPos(i, j).getElementsByClassName(barrierType)[0];
  } else if (direction === 'left') {
    barrier = squareAtPos(i - 1, j).getElementsByClassName(barrierType)[0];
  } else if (direction === 'up') {
    barrier = squareAtPos(i, j - 1).getElementsByClassName(barrierType)[0];
  }
  return barrier && barrier.classList.contains('line');
}
function addFongus(i, j, fongusClass = 'spore') {
  const sporeBox = squareAtPos(i, j).getElementsByClassName('box')[1];
  const currentClass = sporeBox.classList[sporeBox.classList.length - 1];
  if (currentClass === 'bare') {
    sporeBox.classList.remove(currentClass);
  }
  if (['bare', 'fongus'].includes(currentClass)) {
    sporeBox.classList.add(fongusClass);
    return true;
  }
}
function parseTextCoords(textCoords) {
  textCoords = textCoords.trim();
  if (textCoords === 'ligne 1') {
    return [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5]];
  }
  if (textCoords === 'ligne 6') {
    return [[5, 0], [5, 1], [5, 2], [5, 3], [5, 4], [5, 5]];
  }
  if (textCoords === 'colonne A') {
    return [[0, 0], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0]];
  }
  if (textCoords === 'colonne F') {
    return [[0, 5], [1, 5], [2, 5], [3, 5], [4, 5], [5, 5]];
  }
  return textCoords.split('&')
                   .filter(textCoord => textCoord.length === 2)
                   .map(textCoord => parseTextCoord(textCoord.trim()));
}
function parseTextCoord(textCoord) { // A1 -> [0, 0] - F6 -> [5, 5]
  return [Number(textCoord.charAt(1)) - 1, 'ABCDEF'.indexOf(textCoord.charAt(0))];
}
function squareAtPos(i, j) {
  return playerGrid.getElementsByTagName('tr')[i + 1].getElementsByTagName('td')[j + 1];
}
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
function isInSquadPerimeter(i, j) {
    const squadSquare = document.getElementsByClassName('squad')[0];
    console.log(`isInSquadPerimeter: squadSquare=${squadSquare}`)
    return squadSquare && (Math.abs(i - squadSquare.i) + Math.abs(j - squadSquare.j)) <= 1;
}

/******************************************************************************/
/* game balancing stats & analysis                                            */
/******************************************************************************/
function heatmap() {
  // 1: collecting stats
  let sporePerCell = new Proxy({}, { // = defaultdict(dict)
    get: (row, index) => index in row ? row[index] : (row[index]={})
  })
  document.querySelectorAll('#fongus td').forEach(td => {
    parseTextCoords(td.textContent).forEach(([i, j]) => {
      sporePerCell[i][j] = (sporePerCell[i][j] || 0) + 1;
    });
  });
  // 2: drawing the heatmap
  for (let i = 0; i < 6; i++) {
    for (let j = 0; j < 6; j++) {
      squareAtPos(i, j).style.backgroundColor = {
        undefined: 'initial',
        1: 'green',
        2: 'yellow',
        3: 'orange',
        4: 'red',
      }[sporePerCell[i][j]] || 'blue';
    }
  }
}
