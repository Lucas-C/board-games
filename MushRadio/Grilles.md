    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |R |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |F |  |
    +--+--+--+--+--+--+
    |  |R |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |F |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+

- 8 cases DOUBLE ressource
- 8 cases sans AUCUNE ressource, dans les angles

    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |R |  |  |F |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |F |  |  |R |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+

- 0 case DOUBLE ressource
- 0 case sans AUCUNE ressource

    +--+--+--+--+--+--+
    |  |R |  |  |F |  |
    +--+--+--+--+--+--+
    |R |  |  |  |  |F |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |F |  |  |  |  |R |
    +--+--+--+--+--+--+
    |  |F |  |  |R |  |
    +--+--+--+--+--+--+

- 16 cases DOUBLE ressource, dans les angles
- 4 cases sans AUCUNE ressource, au centre

    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |F |  |  |
    +--+--+--+--+--+--+
    |  |  |R |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+

- 4 cases DOUBLE ressource
- 20 cases sans AUCUNE ressource, sur la bordure

    +--+--+--+--+--+--+
    |  |  |  |F |  |  |
    +--+--+--+--+--+--+
    |R |  |  |  |  |F |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |R |  |  |  |  |F |
    +--+--+--+--+--+--+
    |  |  |R |  |  |  |
    +--+--+--+--+--+--+

- 2 cases DOUBLE ressource, dans les angles opposés
- 4 cases sans AUCUNE ressource, au centre


    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+
    |  |  |  |  |  |  |
    +--+--+--+--+--+--+

- ? cases DOUBLE ressource
- ? cases sans AUCUNE ressource
