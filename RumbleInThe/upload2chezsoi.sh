#!/bin/bash

set -o pipefail -o errexit -o nounset -o xtrace
cd $(dirname ${BASH_SOURCE[0]})

for file in *.fodg *.pdf; do
  scp "$file" ct-lucas:~/ludochaordic/content/images/2022/08/
done
