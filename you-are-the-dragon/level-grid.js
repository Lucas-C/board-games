import { borders, gridFromString, levelFromJson } from './units.js';

const SIGILS = 'FGHIMTUVZe';

export default {
  template: '#level-grid',
  props: {
    enlightedPos: Array,
    levelModel: Object,
    levelFilepath: String,
    solution: Array,
  },
  asyncComputed: {
    level: function() {
      if (this.levelModel) {
        return Promise.resolve(this.levelModel);
      } else if (this.levelFilepath) {
        return fetch(this.levelFilepath).then(resp => resp.text())
            .then(levelContent => levelFromJson(levelContent));
      } else {
        const level = levelFromJson('{}');
        level.grid = gridFromString(this.$slots.default[0].text);
        return Promise.resolve(level);
      }
    },
  },
  data() {
    return {
      lastCellClicked: false,
      mouseButtonPressed: false,
    }
  },
  methods: {
    borders(i, j) {
      const borderNames = borders(this.level.grid, i, j);
      return borderNames.length ? `border ${ borderNames.map((borderName) => `border-${ borderName }`).join(' ') }` : '';
    },
    toggleSigil(dir, rowColumnIndex) {
      if (typeof this.level.seal[dir][rowColumnIndex] !== 'undefined') {
        delete this.level.seal[dir][rowColumnIndex];
      } else {
        this.level.seal[dir][rowColumnIndex] = '?';
      }
      this.saveToLocalStorage();
    },
    getSigil(index) {
      return SIGILS[index % SIGILS.length];
    },
    isEnlighted(i, j) {
      return this.enlightedPos && this.enlightedPos.includes(`${ i },${ j }`);
    },
    click(i, j) {
      if (!this.lastCellClicked || (i !== this.lastCellClicked.i && j !== this.lastCellClicked.j)) {
        this.$emit('cell-clicked', {i, j});
      }
      this.lastCellClicked = false;
    },
    mouseButtonPress() {
      this.mouseButtonPressed = true;
    },
    mouseButtonRelease() {
      this.mouseButtonPressed = false;
    },
    mouseMove(i, j) {
      if (this.mouseButtonPressed) {
        this.$emit('cell-clicked', {i, j});
        this.lastCellClicked = {i, j};
      }
    },
  }
};