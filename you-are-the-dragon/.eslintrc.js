module.exports = {
  env: {
    browser: true,
  },
  extends: ['strict', 'plugin:vue/recommended'],
  parserOptions: {
    ecmaVersion: 2019
  },
  globals: {
    Vue: 'readonly',
  },
  rules: {
    'array-element-newline': 'off',
    'id-blacklist': 'off',
    'id-match': ['error', '^(_|[A-Za-z][A-Za-z0-9]*|[A-Z][A-Z_]*[A-Z])$'],
    'id-length': 'off',
    'line-comment-position': 'off',
    'max-len': 'off',
    'no-inline-comments': 'off',
    'no-magic-numbers': ['error', { "ignore": [0, 1, 2, -1] }],
    'no-nested-ternary': 'off',
    'no-new': 'off',
    'no-sync': 'off',
    'no-undefined': 'off',
    'no-warning-comments': 'off',
    'prefer-destructuring': 'off',
  },
};
