<!-- Next:
- https://openclipart.org/image/800px/svg_to_png/313486/1547311829.png ?
- https://pixabay.com/vectors/death-dead-people-zombie-monster-161695/ ?
- améliorer tuto / archers + ex si gob derrière archer
- pdf @print : flip pages 4n+3 / 4n+4
- add bonus question / level : is a gob about to steal the crystal ??
- support unknown knight/archer digit
- ideas for levels:
  * 1 initial gob present
  * gobs display a picture (a heart ?)
  * gobs display the level gobs count
  * hint in title
- game mechanics ideas:
  * https://www.reddit.com/r/puzzles/comments/kamxzp/minesweeper_find_the_only_safe_spot/
  * some cells / passage between walls trigger traps
  * enchantment : if the gobs form a H...
  * extra constraint: there MUST be a path without gobs between A & B
  * should archers indicate a minimum instead ? Ex: 2+
  * placer autre chose que des gobs ?
    + golden gob : compte double -> idée Kevin
  * dragons lives as gold bags ? + needed to "take a chance" to solve it
  * stairs/traps between levels -> idée Laetitia
  * scroll to reveal #gob-count
  * new units: wizard, etc.
  * 1-time usage items to collect ?
  * different characters with special capacities
  * trouver AU MOINS X gobs (mais level a plusieurs solutions) -> idée Nico
  * inspi https://danijmn.itch.io/cardinalchains
- campaign:
  * teach side-by-side knights heuristic
  * personnage à protéger à travers plusieurs niveaux (équivalent d'un K0)
  * énigmes filées à travers niveaux
- story:
  * le repaire de la dragone est une tour gigantesque, le Perchoir
  * histoire: mouvement social des gardes
- add questions to playtesters / level : name + rate difficulty & fun
- solver:
  * improve perfs for cavern-ambush.json (solved in 30s to 60s)
    > Starting iterating with #cellsToTryForGobs=34 - #goblinsPos=0 - #cellsWithoutGobs=10
  * auto-generate levels ?
- add license file to repo
- recommend a free puzzle video game on every grid:
    https://chezsoi.org/shaarli/?searchtags=Puzzle
    https://davelucena.itch.io/castleoftheskullking
- com' : r/Minesweeper, r/puzzles

### Images
Grosse inspi: [Dungeoneering](http://www.guildofdungeoneering.com/), cf. http://blog.gambrinous.com/2013/12/01/the-art-of-dungeoneering/
+ castle: https://www.svgrepo.com/svg/65268/fantasy-castle-hand-drawn-outline
+ castle: https://www.needpix.com/photo/89519/battlement-castle-towers-parapet-turret
+ gobelin with sword: https://pixabay.com/vectors/fantasy-game-asset-call-goblin-1296495/
+ wild knight with sword: https://freesvg.org/wild-knight
+ dragon: https://svgsilh.com/image/1296674.html

### Edition
Idée de contact : https://fr.tipeee.com/leandreproust

Note: je viens de découvrir que des grilles de Démineur/Voisimage ça existe déjà...
-->

### Development

    pip install livereload
    ./watch_and_serve.py

Instructions to build `md2html.bundle.js`: https://github.com/Lucas-C/dotfiles_and_notes/blob/master/bin/md2html.js#L73

### Linter & unit tests

    npm run lint
    npm test
    npm test -- -t "<test description>"

### Assets
#### Illustrations & icônes
- [Adventure Quest icons set](https://www.iconfinder.com/iconsets/fantasy-and-role-play-game-adventure-quest)
CC-BY 3.0 by [Chanut is Industries](https://www.iconfinder.com/Chanut-is)
- [Dragosha dungeon adventure tileset](https://dragosha.com/free/dungeon-part-1-tiles.html)
CC-BY 4.0 by Igor Suntsev
- [game-icons.net](https://game-icons.net) CC0
- [Glitch](https://www.glitchthegame.com/public-domain-game-art/) CC0 1.0
- [Joszs' Medieval Item Pack](https://joszs.itch.io/medieval-item-pack) CC-BY 4.0
#### Polices
- [Mirage Gothic](https://www.dafont.com/mirage-gothic.font?l[]=10&l[]=1) by Carlos Mario Peña Solís
- [L'Engineer](https://www.behance.net/gallery/9274907/LEngineer-Font) by Ferdie Balderas
- [Astro Dot Basic](https://www.dafont.com/astrodotbasic.font?l[]=10&l[]=1) by Robyn Clancy

### Tools used
- Firefox
- Inkscape
- Notepad++
- Python
- Jest test framework
- VueJS
