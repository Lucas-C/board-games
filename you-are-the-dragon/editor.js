/* eslint-disable no-console */
import { arrayRemoveByVal, mapOfIntsToEmptyArrays } from './datastructures.js';
import { levelFromJson, levelToJson, makeCell, setCell, ALL_CELL_TYPES } from './units.js';
import { countGobsInRowOrColumn, dualGrid, getGuardsPositions, getEnlightedPositions, solve } from './solver.js';
import { loadComponentsFromJsAndHtml } from './vuejs-component-loader.js';

const DEFAULT_INITIAL_GRID_SIZE = 6;
const MAX_SOLUTION_LENGTH_EXPECTED = 50;
const SIGILS = 'FGHIMTUVZe';

const flatten = (arrayOfArrays) => Array.prototype.concat.apply([], arrayOfArrays);

loadComponentsFromJsAndHtml(['level-grid']).then(() => {
  window.app = new Vue({
    el: '#app',
    data: {
      level: levelFromJson(localStorage.getItem('YATDCurrentGrid') || '{}'),
      digit: 1,
      availableCellTypes: ALL_CELL_TYPES,
      selectedCellType: 'empty',
      tmpState: {
        solutions: false,
        solutionLengthPicked: 1,
        // The whole hash map must be initialized beforehand for VueJS to work properly:
        solutionIndices: mapOfIntsToEmptyArrays(MAX_SOLUTION_LENGTH_EXPECTED),
        enlightedPos: [],
      },
    },
    created() {
      if (!this.level.grid.length) {
        [ ...Array(DEFAULT_INITIAL_GRID_SIZE) ].forEach(() => this.addRow(DEFAULT_INITIAL_GRID_SIZE));
      }
    },
    computed: {
      levelAsDownloadHref() {
        const blob = new Blob([ levelToJson(this.level) ], { type: 'application/json;charset=utf-8' });
        return URL.createObjectURL(blob);
      },
      solution() {
        if (!this.tmpState.solutions) {
          return [];
        }
        const index = this.tmpState.solutionIndices[Number(this.tmpState.solutionLengthPicked)];
        const solutions = this.tmpState.solutionsPerLength[this.tmpState.solutionLengthPicked];
        if (!solutions || typeof index === 'undefined') {
          return [];
        }
        return solutions[Number(index)];
      },
    },
    methods: {
      addColumn() {
        this.level.grid.forEach((row) => row.push(makeCell(ALL_CELL_TYPES[0], this.digit)));
      },
      removeColumn() {
        this.level.grid.forEach((row) => row.splice(-1, 1));
      },
      addRow(gridWidth) {
        gridWidth = gridWidth || this.level.grid[0].length;
        this.level.grid.push([ ...Array(gridWidth) ].map(() => makeCell(ALL_CELL_TYPES[0], this.digit)));
      },
      removeRow() {
        this.level.grid.splice(-1, 1);
      },
      setContent({i, j}) {
        const cell = this.level.grid[i][j];
        setCell(cell, this.selectedCellType, this.digit);
        this.resetTmpState();
        this.saveToLocalStorage();
      },
      solve() {
        console.time('Solving');
        const solverResults = [ ...solve(this.level.grid) ];
        console.timeEnd('Solving');
        this.level.estimatedDifficulty = solverResults.length === 1 ? solverResults[0].estimatedDifficulty : '';
        this.tmpState.solutions = solverResults.map((result) => result.solution);
        this.updateLevelSeal(this.tmpState.solutions);
        const spl = {};
        this.tmpState.solutions.forEach((solution) => {
          spl[solution.length] = (spl[solution.length] || []).concat([ solution ]);
          this.tmpState.solutionIndices[solution.length] = 0;
        });
        this.tmpState.solutionsPerLength = spl;
      },
      toggleSight() {
        const enlightedPosCount = this.tmpState.enlightedPos.length;
        if (enlightedPosCount) {
          this.tmpState.enlightedPos.splice(0, enlightedPosCount);
        } else {
          getEnlightedPositions(this.level.grid).forEach(([ i, j ]) => {
            this.tmpState.enlightedPos.push(`${ i },${ j }`);
          });
        }
      },
      setDigits() {
        this.findGridWithSingleSolution({ randInit: true, digits: [ 1, 2 ] });
      },
      findGridWithSingleSolution({ digits, randInit = false, allGuardPos, k = 0 }) {
        if (typeof allGuardPos === 'undefined') {
          allGuardPos = [];
          Object.values(getGuardsPositions(this.level.grid)).forEach((intToArrays) => {
            allGuardPos = allGuardPos.concat(flatten(Object.values(intToArrays)));
          });
          if (randInit) {
            for (const [ i, j ] of allGuardPos) {
              this.level.grid[i][j].digit = digits[Math.floor(Math.random() * digits.length)];
            }
          }
        }
        if (k === allGuardPos.length) {
          const solver = solve(this.level.grid);
          // We stop only if a single solution exists.
          // This syntax allow to avoid iterating over all solutions above the 2nd:
          if (!solver.next().done && solver.next().done) {
            this.solve();
            return true;
          }
          return false;
        }
        const [ i, j ] = allGuardPos[k];
        for (const digit of digits) {
          this.level.grid[i][j].digit = digit;
          if (this.findGridWithSingleSolution({ randInit, digits, allGuardPos, k: k + 1 })) {
            return true;
          }
        }
        return false;
      },
      getSigil(index) {
        return SIGILS[index % SIGILS.length];
      },
      updateLevelSeal(solutions) {
        for (const dir of ['horizontal', 'vertical']) {
          for (let index of Object.keys(this.level.seal[dir])) {
            index = Number(index);
            this.level.seal[dir][index] = (solutions.length === 1) ? countGobsInRowOrColumn(solutions[0], dir, index) : '?';
          }
        }
      },
      clearSight() {
        if (this.tmpState.enlightedPos.length) {
          this.tmpState.enlightedPos.splice(0, this.tmpState.enlightedPos.length);
        }
      },
      dual() {
        this.level.grid = dualGrid(this.level.grid, this.tmpState.solutions[0]);
        this.resetTmpState();
      },
      saveToLocalStorage() {
        localStorage.setItem('YATDCurrentGrid', levelToJson(this.level));
      },
      loadLevelFromFile(event) {
        this.resetTmpState();
        const reader = new FileReader();
        const self = this;
        reader.onload = () => {
          self.level = levelFromJson(reader.result);
        };
        reader.readAsText(event.target.files[0]);
      },
      resetTmpState() {
        this.tmpState.solutions = false;
        Object.keys(this.tmpState.solutionIndices).forEach((key) => {
          this.tmpState.solutionIndices[key] = 0;
        });
        this.clearSight();
      },
    },
  });
});