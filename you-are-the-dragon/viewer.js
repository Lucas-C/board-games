import { loadComponentsFromJsAndHtml } from './vuejs-component-loader.js';

loadComponentsFromJsAndHtml(['level-grid']).then(() => {
  Vue.config.silent = true; // Silence [Vue warn]: Error compiling template:
  // Templates should only be responsible for mapping the state to the UI.
  // Avoid placing tags with side-effects in your templates, such as <script>, as they will not be parsed.
  new Vue({
    el: document.getElementsByTagName('section')[0],
  });
  fetch('package.json').then(resp => resp.json()).then(pkg => {
    const versionElem = document.createElement('div');
    versionElem.classList.add('version');
    versionElem.textContent = 'v' + pkg.version;
    document.body.append(versionElem);
  });
});
