export async function loadComponentsFromJsAndHtml(componentNames) {
  return Promise.all(componentNames.map((componentName) =>
    fetch(componentName + '.html').then(resp => resp.text()).then(async (templateHtmlContent) => {
      const script = document.createElement('script');
      script.id = componentName;
      script.type = 'text/x-template';
      script.innerHTML = templateHtmlContent;
      document.body.append(script);
      const componentModule = await import(`./${componentName}.js`);
      Vue.component(componentName, componentModule.default);
      return componentModule;
    })
  ));
}