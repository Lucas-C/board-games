#!/usr/bin/env python3
from glob import glob
from os.path import dirname, splitext
from unittest.mock import patch

from livereload.server import Server, StaticFileHandler


EXTS = ('html', 'css', 'js', 'md', 'vue')
SCRIPT_DIR = dirname(__file__)


class CustomStaticFileHandler(StaticFileHandler):
    'Serves .md files as text.html Content-Type'
    def get_content_type(self):
        if splitext(self.absolute_path)[1] == '.md':
            return 'text/html; charset=utf-8'
        return super().get_content_type()


server = Server()
for filename in sum([glob(SCRIPT_DIR + '/*.' + ext) for ext in EXTS], []):
    print('Watching', filename)
    server.watch(filename)
with patch('livereload.server.StaticFileHandler', CustomStaticFileHandler):
    server.serve(root=SCRIPT_DIR)
