# Vous Êtes La Dragone !
::: dragon
:::

::: symbol
F G H I M T U V Z e
:::

::: intro
Et en tant que dragon, vous veillez jalousement sur votre trésor !

Bien sûr, quelques gobelins avides n'ont pu résister à l'attrait de tout cet or,
et tentent de s'introduire dans votre repaire pour tenter de le dérober...

À vous de les dénicher, en vous basant sur ce que vos gardes vigilants ont repéré !
:::

<div class="l-row">
Ce garde n'a repéré qu'un seul gobelin :
<level-grid>
. . . 
. K1G 
. . . 
</level-grid>
Celui-ci deux :
<level-grid>
. G  .
. K2. 
. G  .
</level-grid>
Et celui-là quatre :
<level-grid>
G . G 
. K4. 
G . G 
</level-grid>
</div>

<div class="l-row">
Cet archer a repéré deux gobelins :
<level-grid>
G G G . 
>2. G G 
</level-grid>
Celui-ci qu'un seul :
<level-grid>
G K2G <1
<level-grid>
</div>

::: intro
Et maintenant, à vous de débusquer les gobelins avec votre crayon !
:::

::: page-break
:::

## La salle du crystal

<level-grid level-filepath="levels/crystal-room.json"></level-grid>

::: page-break
:::

## Symmetrie centrale

<level-grid level-filepath="levels/central-symmetry.json"></level-grid>

::: page-break
:::

## En formation

<level-grid level-filepath="levels/guards-in-formation.json"></level-grid>

## Infestation

<level-grid level-filepath="levels/infestation.json"></level-grid>

::: page-break
:::

## Cathédrale

<level-grid level-filepath="levels/cathedral.json"></level-grid>

::: page-break
:::

## Ambuscade dans la caverne

<level-grid level-filepath="levels/cavern-ambush.json"></level-grid>

<script src="node_modules/vue/dist/vue.js"></script>
<script src="node_modules/vue-async-computed/dist/vue-async-computed.js"></script>
<script src="md2html.bundle.js"></script>
<script type="module" src="viewer.js"></script>
<link rel="stylesheet" type="text/css" href="yatd.css">
