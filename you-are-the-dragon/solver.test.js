/* global expect, test*/
/* eslint no-magic-numbers: "off" */
import fs from 'fs';
import { gridFromString } from './units.js';
import { getEnlightedPositions, getSideBySideKnights, solve, SolverResult } from './solver.js';

test('getEnlightedPositions correctly return a single cell covered by several guards', () => {
  const grid = `K1R.K1
                R...R.
                K1R.K1`;
  expect(getEnlightedPositions(gridFromString(grid))).toStrictEqual([ [ 1, 1 ] ]);
});

test('getEnlightedPositions with archers of each direction', () => {
  const grid = `. . . . . . . 
                . . . . . . . 
                . . . ^1. . . 
                . . <1. >1. . 
                . . . v1. . . 
                . . . . . . . 
                . . . . . . . `;
  expect(getEnlightedPositions(gridFromString(grid))).toStrictEqual([
    [ 3, 5 ], [ 3, 6 ],
    [ 5, 3 ], [ 6, 3 ],
    [ 3, 1 ], [ 3, 0 ],
    [ 1, 3 ], [ 0, 3 ],
  ]);
});

test('getSideBySideKnights', () => {
  const grid = `K1. K2K2
                K1. . . 
                . . . K3
                K4K4. K3`;
  expect([ ...getSideBySideKnights(gridFromString(grid)) ]).toStrictEqual([
    [ [ 0, 0 ], [ 1, 0 ] ],
    [ [ 0, 2 ], [ 0, 3 ] ],
    [ [ 2, 3 ], [ 3, 3 ] ],
    [ [ 3, 0 ], [ 3, 1 ] ],
  ]);
});

test('solve with knights, rocks and a single possible solution', () => {
  const grid = `K1R.K1
                R...R.
                K1R.K1`;
  expect([ ...solve(gridFromString(grid)) ]).toStrictEqual([ new SolverResult([ '1,1' ], 0) ]);
});

test('solve with 3 possible solutions and knights with 2 sights', () => {
  const grid = `K2..K2
                ......
                K2..K2`;
  expect([ ...solve(gridFromString(grid)) ]).toIncludeSameMembers([
    new SolverResult([ '0,1', '1,0', '1,2', '2,1' ], 7),
    new SolverResult([ '0,1', '1,1', '2,1' ], 7),
    new SolverResult([ '1,0', '1,1', '1,2' ], 7),
  ]);
});

test('solve with a single solution and only archers', () => {
  const grid = `>0....
                ....<2
                >0....`;
  expect([ ...solve(gridFromString(grid)) ]).toIncludeSameMembers([ new SolverResult([ '1,0', '1,1' ], 0) ]);
});

test('solve Crystal Room', () => {
  const level = JSON.parse(fs.readFileSync('levels/crystal-room.json'));
  expect([ ...solve(level.grid) ]).toIncludeSameMembers([
    new SolverResult([
      '1,4', '10,1', '10,2', '10,3', '10,5', '10,6', '10,7', '2,1', '2,2', '2,3', '2,5', '2,6', '2,7',
      '4,1', '4,2', '4,3', '4,5', '4,6', '4,7', '5,4', '6,1', '6,3', '6,4', '6,5', '6,7', '7,4',
      '8,1', '8,2', '8,3', '8,5', '8,6', '8,7',
    ], 4),
  ]);
});

test('solve Cathedral', () => {
  const level = JSON.parse(fs.readFileSync('levels/cathedral.json'));
  expect([ ...solve(level.grid) ]).toIncludeSameMembers([ new SolverResult([ '1,2', '1,3', '1,4', '1,5', '2,4', '2,6', '2,7', '2,9', '3,1', '3,4', '3,6', '4,3', '4,7', '5,4', '5,6', '6,1', '6,2', '6,3', '6,5', '6,7', '6,8', '6,9', '7,1', '8,4', '8,6', '8,7', '8,9', '9,4' ], 85) ]);
});

test('solve Cavern Ambush', () => {
  const level = JSON.parse(fs.readFileSync('levels/cavern-ambush.json'));
  expect([ ...solve(level.grid) ]).toIncludeSameMembers([ new SolverResult([ '1,10', '1,2', '1,5', '1,6', '1,9', '3,3', '3,4', '3,5', '3,7', '3,8', '3,9', '6,3', '6,9', '8,5', '8,7' ], 25) ]);
});

test('solve Difficult Indecision', () => {
  const level = JSON.parse(fs.readFileSync('levels/difficult-indecision.json'));
  expect([ ...solve(level.grid) ]).toIncludeSameMembers([ new SolverResult([ '1,3', '3,1', '3,5', '5,2', '6,3' ], 20) ]);
});
