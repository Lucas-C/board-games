/*
 * In order to be able to store coordinates in a Set,
 * they are represented as a string 'X,Y'.
*/
import { arrayRemoveByVal, deepCopy, mapOfIntsToEmptyArrays } from './datastructures.js';

const KNIGHT_NEIGHBOURS_COUNT = 8;
const ARCHER_MAX_SIGHT = 20;
const SIDE_BY_SIDE_KNIGHT_EQUAL_VALUE_DIFFICULTY_BUFF = 15;
const SIDE_BY_SIDE_KNIGHT_DIFFERING_VALUE_DIFFICULTY_BUFF = 10;

const POS_INCR_PER_ARCHER_TYPE = {
  'archer-right': { x: 0, y: 1 },
  'archer-down': { x: 1, y: 0 },
  'archer-left': { x: 0, y: -1 },
  'archer-up': { x: -1, y: 0 },
};
const OPPOSITE_DIRECTION = {
  'up': 'down',
  'down': 'up',
  'right': 'left',
  'left': 'right',
};

function strPosToArray(pos) {
  return pos.split(',').map((i) => parseInt(i, 10));
}

function emptyNeighbourPos(grid, [ i, j ]) {
  const closePos = [
    [ i - 1, j - 1 ], [ i - 1, j ], [ i - 1, j + 1 ],
    [ i, j - 1 ], [ i, j + 1 ],
    [ i + 1, j - 1 ], [ i + 1, j ], [ i + 1, j + 1 ],
  ];
  const pos = [];
  for (const [ x, y ] of closePos) {
    if (x >= 0 && x < grid.length && y >= 0 && y < grid[0].length && grid[x][y].type === 'empty') {
      pos.push([ x, y ]);
    }
  }
  return pos;
}

function emptyNeighbourPosInDirection(grid, [ i, j ], dir) {
  const closePosPerDir = {
    'up': [ [ i - 1, j - 1 ], [ i - 1, j ], [ i - 1, j + 1 ] ],
    'down': [ [ i + 1, j - 1 ], [ i + 1, j ], [ i + 1, j + 1 ] ],
    'left': [ [ i - 1, j - 1 ], [ i, j - 1 ], [ i + 1, j - 1 ] ],
    'right': [ [ i - 1, j + 1 ], [ i, j + 1 ], [ i + 1, j + 1 ] ],
  };
  const pos = [];
  for (const [ x, y ] of closePosPerDir[dir]) {
    if (x >= 0 && x < grid.length && y >= 0 && y < grid[0].length && grid[x][y].type === 'empty') {
      pos.push(`${ x },${ y }`);
    }
  }
  return pos;
}

// Note: archers cannot see beyond other guards
function archerSightPos(grid, type, [ i, j ]) {
  const { x, y } = POS_INCR_PER_ARCHER_TYPE[type];
  const pos = [];
  [ i, j ] = [ i + x, j + y ];
  while (i >= 0 && i < grid.length && j >= 0 && j < grid[0].length && grid[i][j].type === 'empty') {
    pos.push(`${ i },${ j }`);
    [ i, j ] = [ i + x, j + y ];
  }
  return pos;
}

function guardPosInSight(grid, guardType, guardPos) {
  return guardType === 'knight' ?
    emptyNeighbourPos(grid, guardPos).map(([ x, y ]) => `${ x },${ y }`) :
    archerSightPos(grid, guardType, guardPos);
}

function enlightedPositionsFrom(grid, guardsPosPerType) {
  const pos = new Set();
  for (const guardType of Object.keys(guardsPosPerType)) {
    for (const guardsPos of Object.values(guardsPosPerType[guardType])) {
      for (const guardPos of guardsPos) {
        for (const strPos of guardPosInSight(grid, guardType, guardPos)) {
          pos.add(strPos);
        }
      }
    }
  }
  return pos;
}

export function getGuardsPositions(grid) {
  const pos = {
    'knight': mapOfIntsToEmptyArrays(KNIGHT_NEIGHBOURS_COUNT + 1),
    'archer-right': mapOfIntsToEmptyArrays(ARCHER_MAX_SIGHT),
    'archer-down': mapOfIntsToEmptyArrays(ARCHER_MAX_SIGHT),
    'archer-left': mapOfIntsToEmptyArrays(ARCHER_MAX_SIGHT),
    'archer-up': mapOfIntsToEmptyArrays(ARCHER_MAX_SIGHT),
  };
  const posKeys = Object.keys(pos);
  grid.forEach((row, i) => row.forEach((cell, j) => {
    if (posKeys.includes(cell.type)) {
      pos[cell.type][cell.digit].push([ i, j ]);
    }
  }));
  return pos;
}
function getGobsPositions(grid) {
  const pos = [];
  grid.forEach((row, i) => row.forEach((cell, j) => {
    if (cell.type === 'gob') {
      pos.push(`${ i },${ j }`);
    }
  }));
  return pos;
}

export function * getSideBySideKnights(grid) {
  for (let i = 0; i < grid.length; i++) {
    for (let j = 0; j < grid[0].length; j++) {
      if (grid[i][j].type === 'knight') {
        if (i + 1 < grid.length && grid[i + 1][j].type === 'knight') {
          yield [ [ i, j ], [ i + 1, j ] ];
        }
        if (j + 1 < grid[0].length && grid[i][j + 1].type === 'knight') {
          yield [ [ i, j ], [ i, j + 1 ] ];
        }
      }
    }
  }
}

function allGuardsSeeTheCorrectAmountOfGobs(grid, guardsPos, goblinsPos) {
  for (const guardType of Object.keys(guardsPos)) {
    for (const gobsSeen of Object.keys(guardsPos[guardType])) {
      for (const guardPos of guardsPos[guardType][gobsSeen]) {
        const gobsInSightCount = guardPosInSight(grid, guardType, guardPos).filter((strPos) => goblinsPos.includes(strPos)).length;
        if (gobsInSightCount !== Number(gobsSeen)) {
          return false;
        }
      }
    }
  }
  return true;
}

export class SolverResult {
  constructor(solution, estimatedDifficulty) {
    this.solution = solution;
    this.estimatedDifficulty = estimatedDifficulty;
  }
}

/* eslint-disable max-depth */
// eslint-disable-next-line complexity, max-depth, max-statements
function * recurSolve(grid, { guardsPos, cellsToTryForGobs, goblinsPos, cellsWithoutGobs, iterationDepth = 0, estimatedDifficulty = 0 } = {}) {
  // console.log(`Entering recurSolve with goblinsPos=${goblinsPos} cellsWithoutGobs=${cellsWithoutGobs && Array.from(cellsWithoutGobs)}`);
  if (typeof guardsPos === 'undefined') {
    guardsPos = getGuardsPositions(grid);
  }
  if (typeof cellsToTryForGobs === 'undefined') {
    cellsToTryForGobs = Array.from(enlightedPositionsFrom(grid, guardsPos));
  }
  if (typeof goblinsPos === 'undefined') {
    goblinsPos = getGobsPositions(grid); // Using an Array and not a Set to benefit from push / pop methods,
    // and keeping the pos traversal deterministic & ordered
  }
  cellsWithoutGobs = new Set(cellsWithoutGobs); // copy or create it empty
  let newGoblinsPos = goblinsPos;
  // 1. We try to find a guard who has as many empty cells in sight as their are seeing gobs:
  let guessMade = false;
  for (const guardType of Object.keys(guardsPos)) {
    for (const gobsSeen of Object.keys(guardsPos[guardType])) {
      for (const guardPos of guardsPos[guardType][gobsSeen]) {
        const posInSight = guardPosInSight(grid, guardType, guardPos);
        const emptyPosInSight = posInSight.filter((strPos) => !goblinsPos.includes(strPos) && !cellsWithoutGobs.has(strPos));
        const gobsInSightCount = posInSight.filter((strPos) => goblinsPos.includes(strPos)).length;
        // console.log(`DEBUG: guardPos=${guardPos} posInSight=${posInSight} gobsSeen=${gobsSeen} emptyPosInSight=${emptyPosInSight} gobsInSightCount=${gobsInSightCount} goblinsPos=${goblinsPos} cellsToTryForGobs=${cellsToTryForGobs} cellsWithoutGobs=${Array.from(cellsWithoutGobs)}`);
        if (!emptyPosInSight.length) {
          // We avoid an infinite loop: zero position in sight is still candidate,
          // no point in going further
          continue;
        }
        if (emptyPosInSight.length === Number(gobsSeen) - gobsInSightCount) {
          // => There are exactly has may empty cells in sight as the guard is seeing gobs:
          newGoblinsPos = Array.from(goblinsPos);
          for (const newGoblinPos of emptyPosInSight) {
            newGoblinsPos.push(newGoblinPos);
            arrayRemoveByVal(cellsToTryForGobs, newGoblinPos);
          }
          guessMade = true;
        } else if (gobsInSightCount === Number(gobsSeen)) {
          // => There are exactly as many gobs guessed as the guard is seeing,
          // and some of those cells are still "candidates" -> we remove them
          for (const cellWithoutGob of emptyPosInSight) {
            cellsWithoutGobs.add(cellWithoutGob);
            arrayRemoveByVal(cellsToTryForGobs, cellWithoutGob);
          }
          guessMade = true;
        }
        if (guessMade) {
          // console.log(`Exact guard match! guardPos=${guardPos} gobsSeen=${gobsSeen} emptyPosInSight=${emptyPosInSight} gobsInSightCount=${gobsInSightCount} newGoblinsPos=${newGoblinsPos} cellsToTryForGobs=${cellsToTryForGobs} cellsWithoutGobs=${Array.from(cellsWithoutGobs)}`);
          if (!cellsToTryForGobs.length && allGuardsSeeTheCorrectAmountOfGobs(grid, guardsPos, newGoblinsPos)) {
            yield new SolverResult(Array.from(newGoblinsPos), estimatedDifficulty + iterationDepth);
          } else {
            yield * recurSolve(grid, { guardsPos, cellsToTryForGobs, goblinsPos: newGoblinsPos, cellsWithoutGobs, iterationDepth, estimatedDifficulty });
          }
          return;
        }
      }
    }
  }
  if (!iterationDepth) {
    // 2: heuristic for side-by-side knights, done once only
    for (const [ [ k1x, k1y ], [ k2x, k2y ] ] of getSideBySideKnights(grid)) {
      const k1Digit = grid[k1x][k1y].digit;
      const k2Digit = grid[k2x][k2y].digit;
      // Relative position of k1 compared to k2
      const dir1 = (k1x === k2x) ?
        (k1y > k2y ? 'right' : 'left') :
        (k1x > k2x ? 'down' : 'up');
      const dir2 = OPPOSITE_DIRECTION[dir1];
      const emptyCellsInDir1 = emptyNeighbourPosInDirection(grid, [ k1x, k1y ], dir1);
      const emptyCellsInDir2 = emptyNeighbourPosInDirection(grid, [ k2x, k2y ], dir2);
      if (k1Digit === k2Digit) {
        // console.log(`${emptyCellsInDir1.length + emptyCellsInDir2.length} cells without gobs guessed from knight pair ${[ k1x, k1y ]}, ${[ k2x, k2y ]} with digit ${k1Digit}`);
        for (const pos of emptyCellsInDir1) {
          cellsWithoutGobs.add(pos);
        }
        for (const pos of emptyCellsInDir2) {
          cellsWithoutGobs.add(pos);
        }
        guessMade = true;
        estimatedDifficulty += SIDE_BY_SIDE_KNIGHT_EQUAL_VALUE_DIFFICULTY_BUFF;
      } else if (k1Digit - k2Digit === emptyCellsInDir1.length || k2Digit - k1Digit === emptyCellsInDir2.length) {
        let emptyCellsToPopulate = (k1Digit - k2Digit === emptyCellsInDir1.length) ? emptyCellsInDir1 : emptyCellsInDir2;
        emptyCellsToPopulate = emptyCellsToPopulate.filter((strPos) => !goblinsPos.includes(strPos));
        if (emptyCellsToPopulate.length) {
          // console.log(`${emptyCellsToPopulate.length} goblins positions guessed from knight pair ${[ k1x, k1y ]} (${k1Digit}), ${[ k2x, k2y ]} (${k2Digit})`);
          if (!guessMade) {
            newGoblinsPos = Array.from(newGoblinsPos);
          }
          for (const pos of emptyCellsToPopulate) {
            newGoblinsPos.push(pos);
          }
          guessMade = true;
          estimatedDifficulty += SIDE_BY_SIDE_KNIGHT_DIFFERING_VALUE_DIFFICULTY_BUFF;
        }
      }
    }
    console.debug(`Starting iterating with #cellsToTryForGobs=${ cellsToTryForGobs.length } - #goblinsPos=${ newGoblinsPos.length } - #cellsWithoutGobs=${ cellsWithoutGobs.size }`);
    estimatedDifficulty += Math.max(0, cellsToTryForGobs.length - cellsWithoutGobs.size);
    if (guessMade) {
      // We trigger iteration by setting incrementing iterationDepth in order to avoid a recursion
      yield * recurSolve(grid, { guardsPos, cellsToTryForGobs, goblinsPos: newGoblinsPos, cellsWithoutGobs, iterationDepth: iterationDepth + 1, estimatedDifficulty });
      return;
    }
  }
  // 3. Else we iterate all empty cells:
  iterationDepth++;
  for (let k = 0; k < cellsToTryForGobs.length; k++) {
    const goblinPos = cellsToTryForGobs[k];
    if (cellsWithoutGobs.has(goblinPos) || goblinsPos.includes(goblinPos)) {
      continue;
    }
    goblinsPos.push(goblinPos);
    const remainingCellsToTryForGobs = cellsToTryForGobs.slice(k + 1);
    let allGobsSeenInSight = true;
    let anyGuardHasTooManyGobsInSight = false;
    for (const guardType of Object.keys(guardsPos)) {
      if (anyGuardHasTooManyGobsInSight) {
        break;
      }
      for (const gobsSeen of Object.keys(guardsPos[guardType])) {
        if (anyGuardHasTooManyGobsInSight) {
          break;
        }
        for (const guardPos of guardsPos[guardType][gobsSeen]) {
          if (anyGuardHasTooManyGobsInSight) {
            break;
          }
          const posInSight = guardPosInSight(grid, guardType, guardPos);
          const gobsInSight = posInSight.filter((strPos) => goblinsPos.includes(strPos));
          if (gobsInSight.length !== Number(gobsSeen)) {
            allGobsSeenInSight = false;
          }
          if (gobsInSight.length > Number(gobsSeen)) {
            anyGuardHasTooManyGobsInSight = true;
          }
        }
      }
    }
    // console.log(`goblinPos=${goblinPos} anyGuardHasTooManyGobsInSight=${anyGuardHasTooManyGobsInSight} remainingCellsToTryForGobs=${remainingCellsToTryForGobs} cellsWithoutGobs=${Array.from(cellsWithoutGobs)}`);
    if (!anyGuardHasTooManyGobsInSight) {
      if (allGobsSeenInSight) {
        yield new SolverResult(Array.from(goblinsPos), estimatedDifficulty + iterationDepth);
      } else if (remainingCellsToTryForGobs.length) {
        yield * recurSolve(grid, { guardsPos, cellsToTryForGobs: remainingCellsToTryForGobs, goblinsPos, cellsWithoutGobs, iterationDepth, estimatedDifficulty });
      }
    }
    goblinsPos.pop();
  }
}

// Ensure solutions uniqueness:
export function * solve(grid) {
  const estimatedDifficultyPerSolution = {};
  for (const result of recurSolve(grid)) {
    result.solution.sort();
    estimatedDifficultyPerSolution[result.solution.join('|')] = result.estimatedDifficulty;
  }
  for (const solution of Object.keys(estimatedDifficultyPerSolution)) {
    yield new SolverResult(solution.split('|'), estimatedDifficultyPerSolution[solution]);
  }
}

export function getEnlightedPositions(grid) {
  return Array.from(enlightedPositionsFrom(grid, getGuardsPositions(grid))).map(strPosToArray);
}

export function dualGrid(grid, goblinsPos) {
  const newGrid = deepCopy(grid);
  const guardsPos = getGuardsPositions(grid);
  for (const guardType of Object.keys(guardsPos)) {
    for (const gobsSeen of Object.keys(guardsPos[guardType])) {
      for (const guardPos of guardsPos[guardType][gobsSeen]) {
        const posInSight = guardPosInSight(grid, guardType, guardPos);
        const gobsInSightCount = posInSight.filter((strPos) => goblinsPos.includes(strPos)).length;
        const [ i, j ] = guardPos;
        newGrid[i][j].digit = posInSight.length - gobsInSightCount;
      }
    }
  }
  return newGrid;
}

export function countGobsInRowOrColumn(goblinsPos, dir, index) {
  return goblinsPos.map(strPosToArray).filter(([i, j]) => (dir === 'horizontal') ? i === index : j === index).length;
}
