export function arrayRemoveByVal(array, val) {
  const index = array.indexOf(val);
  if (index > -1) {
    array.splice(index, 1);
  }
}

export function deepCopy(grid) {
  return JSON.parse(JSON.stringify(grid));
}

export function mapOfIntsToEmptyArrays(count) {
  return [ ...Array(count) ].reduce((map, _, i) => {
    map[i] = [];
    return map;
  }, {});
}
