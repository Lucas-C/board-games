const CELL_TYPE_WITH_DIGIT = [ 'knight', 'archer-right', 'archer-down', 'archer-left', 'archer-up' ];
export const ALL_CELL_TYPES = [
  'empty', ...CELL_TYPE_WITH_DIGIT, 'gob', 'crystal', 'map', 'egg', 'food', 'death', 'bag', 'king',
  'medusa', 'spider', 'potion', 'sorceress', 'spell-book', 'scroll', 'tree', 'werewolf',
  'barrel', 'box-with-water-bottle', 'chest', 'door', 'grating', 'ladder-down', 'monolith', 'skeleton', 'skulls', 'stone-paved', 'throne',
  'cauldron', 'practice-target',
  'document', 'feather', 'alt-potion',
  'floor-trap', 'eye-right', 'rock', 'tripwire', 'wolf-trap',  // TODO: rename "rock" into "wall"
  'stairs', 'none',
];
const ABBREVS = {
  '.': 'empty',
  'K': 'knight',
  '>': 'archer-right',
  'v': 'archer-down',
  '<': 'archer-left',
  '^': 'archer-up',
  'G': 'gob',
  'C': 'crystal',
  'R': 'rock',
};

export function makeCell(type, digit) {
  return { type, digit: CELL_TYPE_WITH_DIGIT.includes(type) ? Number(digit) : '' };
}

export function setCell(cell, type, digit) {
  cell.type = type;
  cell.digit = (CELL_TYPE_WITH_DIGIT.includes(type)) ? Number(digit) : '';
}

export function gridFromString(strGrid) {
  const grid = [];
  for (let line of strGrid.split('\n')) {
    line = line.trim();
    if (line) {
      const row = [];
      for (let i = 0; i < line.length; i += 2) {
        row.push(makeCell(ABBREVS[line.substring(i, i + 1)], line.substring(i + 1, i + 2)));
      }
      grid.push(row);
    }
  }
  return grid;
}

export function levelToJson(level) {
  level.lastUpdate = new Date().toISOString();
  return JSON.stringify(level, null, 2);
}

export function levelFromJson(jsonString) {
  const level = JSON.parse(jsonString);
  level.grid = level.grid || [];
  level.seal = level.seal || {};
  // We transform string indices of level.seal.$dir into integers:
  for (const dir of ['horizontal', 'vertical']) {
    const newSealForDir = {};
    for (const index of Object.keys(level.seal[dir] || {})) {
      newSealForDir[Number(index)] = level.seal[dir][index];
    }
    level.seal[dir] = newSealForDir;
  }
  return level;
}

export function borders(grid, i, j) {
  if (grid[i][j].type === 'none') {
    return [];
  }
  const borderNames = [];
  if (i === 0 || grid[i - 1][j].type === 'none') {
    borderNames.push('top');
  }
  if (i === grid.length - 1 || grid[i + 1][j].type === 'none') {
    borderNames.push('bottom');
  }
  if (j === 0 || grid[i][j - 1].type === 'none') {
    borderNames.push('left');
  }
  if (j === grid[0].length - 1 || grid[i][j + 1].type === 'none') {
    borderNames.push('right');
  }
  return borderNames;
}
