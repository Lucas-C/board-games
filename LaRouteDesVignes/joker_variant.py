#!/usr/bin/env python3

# INSTALL: pip install fpdf2
# USAGE: ./to_pdf.py [EN|FR|PrinterFriendly]

import sys
from fpdf import FPDF, HTMLMixin

class PDF(FPDF, HTMLMixin):
    pass


TITLE_PER_LANG = {  # lang -> (size, text)
    "EN": (60, "AVENUE"),
    "FR": (40, "LA ROUTE DES VIGNES"),
}
RULES_PER_LANG = {
    "EN": """
            <b>Variant: JOKER</b><br><br>
            Once per game, you can ignore the card drawn and pick one:<ul>
            <li><b>crossroad</b>: place an orthogonal cross anywhere on your grid;</li>
            <li><b>mildiew</b>: choose a cell without any building nor path:</li></ul><br>
            all players that have this cell empty on their grid must draw a diagonal cross on it,<br>
            preventing any further path to be traced. You must also draw this cross.""",
    "FR": """
            <b>Variante : JOKER</b><br><br>
            Une fois par partie, vous pouvez ignorer la carte tirée et tracer au choix :<ul>
            <li><b>croisement</b>: placez une intersection orthogonale sur n'importe quelle case</li>
            <li><b>mildiou</b>: choississez une case sans bâtiment ni chemin tracé :</li></ul><br>
            tous les joueurs qui n'ont rien dans cette case doivent y tracer une croix diagonale,<br>
            qui empêche toute route de passer. Vous devez également tracer cette croix.""",
}
FOOTER_PER_LANG = {
    "EN": ('Variant by <a href="https://chezsoi.org/lucas/blog/">Lucas Cimon</a> for the game <a href="https://boardgamegeek.com/boardgame/205045/avenue/files">Avenue</a>', 'Title font: Arreial Black by Alvaro Thomaz'),
    "FR": ('Variante par <a href="https://chezsoi.org/lucas/blog/">Lucas Cimon</a> pour le jeu <a href="https://boardgamegeek.com/boardgame/205045/avenue/files">La Route Des Vignes</a>', 'Police du titre : Arreial Black par Alvaro Thomaz'),
}
PDF_FILENAME_PER_MODE = {
    "EN":              "Avenue-Joker-Variant-EN.pdf",
    "FR":              "LaRouteDesVignes-Variante-Joker-FR.pdf",
    "PrinterFriendly": "Avenue-LaRouteDesVignes-Joker-Variant-PrinterFriendly.pdf",
}


def main(mode):
    pdf = PDF(li_tag_indent=5)
    pdf.set_margin(0)
    pdf.add_page()
    if mode == "PrinterFriendly":
        # Main grid images:
        pdf.image("joker-variant-grid.png", x=35, h=pdf.eph /2 - 10, y=10)
        pdf.image("joker-variant-grid.png", x=35, h=pdf.eph /2 - 10, y=pdf.eph /2)
        # Footer:
        footer_lines = list(FOOTER_PER_LANG["EN"])
        footer_lines.pop()  # removing credit line for title font
        with pdf.rotation(90, .85*pdf.epw, pdf.eph/2):
            render_footer(pdf, footer_lines, pdf.epw/2, pdf.eph/2)
    else:
        # Title:
        title_size, title_text = TITLE_PER_LANG[mode]
        pdf.add_font("ArreialBlack", fname="ArreialBlack.ttf", uni=True)
        pdf.set_font("ArreialBlack", size=title_size)
        pdf.set_text_color(r=38, g=150, b=190)
        pdf.y = 25
        pdf.multi_cell(w=pdf.epw, h=0, txt=title_text, align="C")
        # Rules:
        pdf.set_font("Helvetica", size=14)
        pdf.set_text_color(0)  # black
        pdf.y = 40
        pdf.write_html(RULES_PER_LANG[mode])
        # Main grid image:
        pdf.image("joker-variant-grid.png", x=10, y=85, w=pdf.epw - 20)
        # Footer:
        render_footer(pdf, FOOTER_PER_LANG[mode], 100, 280)
    pdf.output(PDF_FILENAME_PER_MODE[mode])
    print("Sucessfully generated:", PDF_FILENAME_PER_MODE[mode])


def render_footer(pdf, footer_lines, footer_x, footer_y):
    pdf.set_font("Helvetica", size=10)
    for i, footer_line in enumerate(footer_lines):
        pdf.set_xy(footer_x, footer_y + 5*i)
        pdf.write_html(footer_line)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        mode = sys.argv[1]
        assert mode in PDF_FILENAME_PER_MODE
        main(mode)
    else:
        for mode in PDF_FILENAME_PER_MODE:
            main(mode)
