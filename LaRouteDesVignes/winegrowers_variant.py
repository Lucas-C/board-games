#!/usr/bin/env python3

# INSTALL: pip install fpdf2
# USAGE: ./to_pdf.py [EN|FR|PrinterFriendly]

import sys
from fpdf import FPDF, HTMLMixin

class PDF(FPDF, HTMLMixin):
    pass


TITLE_PER_LANG = {  # lang -> (size, text)
    "EN": (60, "AVENUE"),
    "FR": (40, "LA ROUTE DES VIGNES"),
}
RULES_PER_LANG = {
    "EN": """
            <b>Variant: WINE-GROWERS</b><br><br>
            Beware not to mix the harvests of different wine-growers!<br>
            You lose 10 points for every pair of wine-growers connected.""",
    "FR": """
            <b>Variante : VIGNERONS</b><br><br>
            Attention à ne pas facher les vignerons en mélangeant leurs récoltes !<br>
            Vous perdez 10 points pour chaque paire de vignerons reliés.""",
}
FOOTER_PER_LANG = {
    "EN": ('Variant by <a href="https://chezsoi.org/lucas/blog/">Lucas Cimon</a> for the game <a href="https://boardgamegeek.com/boardgame/205045/avenue/files">Avenue</a>', 'Wine-grower icon from <a href="https://loading.io/icon/v7mq42">loading.io</a>', 'Title font: Arreial Black by Alvaro Thomaz'),
    "FR": ('Variante par <a href="https://chezsoi.org/lucas/blog/">Lucas Cimon</a> pour le jeu <a href="https://boardgamegeek.com/boardgame/205045/avenue/files">La Route Des Vignes</a>', 'Icône de vigneron issue de <a href="https://loading.io/icon/v7mq42">loading.io</a>', 'Police du titre : Arreial Black par Alvaro Thomaz'),
}
PDF_FILENAME_PER_MODE = {
    "EN":              "Avenue-Winegrowers-Variant-EN.pdf",
    "FR":              "LaRouteDesVignes-Variante-Vignerons-FR.pdf",
    "PrinterFriendly": "Avenue-LaRouteDesVignes-Winegrowers-Variant-PrinterFriendly.pdf",
}


def main(mode):
    pdf = PDF()
    pdf.set_margin(0)
    pdf.add_page()
    if mode == "PrinterFriendly":
        # Main grid images:
        pdf.image("winegrowers-variant-grid.png", x=35, h=pdf.eph /2 - 10, y=10)
        pdf.image("winegrowers-variant-grid.png", x=35, h=pdf.eph /2 - 10, y=pdf.eph /2)
        # Footer:
        footer_lines = list(FOOTER_PER_LANG["EN"])
        footer_lines.pop()  # removing credit line for title font
        with pdf.rotation(90, .85*pdf.epw, pdf.eph/2):
            render_footer(pdf, footer_lines, pdf.epw/2, pdf.eph/2)
    else:
        # Title:
        title_size, title_text = TITLE_PER_LANG[mode]
        pdf.add_font("ArreialBlack", fname="ArreialBlack.ttf", uni=True)
        pdf.set_font("ArreialBlack", size=title_size)
        pdf.set_text_color(r=38, g=150, b=190)
        pdf.y = 25
        pdf.multi_cell(w=pdf.epw, h=0, txt=title_text, align="C")
        # Rules:
        pdf.set_font("Helvetica", size=14)
        pdf.set_text_color(0)  # black
        pdf.y = 40
        pdf.write_html(RULES_PER_LANG[mode])
        # Main grid image:
        pdf.image("winegrowers-variant-grid.png", x=10, y=80, w=pdf.epw - 20)
        # Footer:
        render_footer(pdf, FOOTER_PER_LANG[mode], 100, 275)
    pdf.output(PDF_FILENAME_PER_MODE[mode])
    print("Sucessfully generated:", PDF_FILENAME_PER_MODE[mode])


def render_footer(pdf, footer_lines, footer_x, footer_y):
    pdf.set_font("Helvetica", size=10)
    for i, footer_line in enumerate(footer_lines):
        pdf.set_xy(footer_x, footer_y + 5*i)
        pdf.write_html(footer_line)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        mode = sys.argv[1]
        assert mode in PDF_FILENAME_PER_MODE
        main(mode)
    else:
        for mode in PDF_FILENAME_PER_MODE:
            main(mode)
