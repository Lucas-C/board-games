
# Variantes pour La Route Des Vignes

_cf._ https://chezsoi.org/lucas/blog/deux-variantes-pour-la-route-des-vignes.html

Les fichiers PDF peuvent être produits par les scripts Python,
une fois les image PNG de chaque grille générées à partir de leur `.xcf` avec [Gimp](https://www.gimp.org).

<!--
cp *Variant*.pdf /opt/ludochaordic/content/images/jeux/
scp *Variant*.pdf ct-lucas:ludochaordic/content/images/jeux/
-->
